package az.ingress.ms9;

import az.ingress.ms9.model.Student;
import az.ingress.ms9.model.Student$;
import az.ingress.ms9.repository.StudentRepository;
import com.speedment.jpastreamer.application.JPAStreamer;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import javax.persistence.EntityManagerFactory;
import java.util.Set;

@SpringBootApplication
@EnableConfigurationProperties
@RequiredArgsConstructor
public class Ms9Application implements CommandLineRunner {
    private final EntityManagerFactory entityManagerFactory;
    private final StudentRepository studentRepository;

    public static void main(String[] args) {
        SpringApplication.run(Ms9Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

//        SQL
//        studentRepository.sqlFindStudentByAddressAndSurname("ov","Baki")
//                .stream().forEach(System.out::println);
//        JPQL
        studentRepository.jpqlFindStudentByAddressAndSurname("ov", "Baki")
        .stream().forEach(System.out::println);

        //JPA Streamer
        JPAStreamer jpaStreamer = JPAStreamer.of(entityManagerFactory);
        jpaStreamer.stream(Student.class)
                .filter(Student$.surname.endsWith("ov"))
                .filter(Student$.address.contains("Baki"))
                .forEach(System.out::println);


//    Student student = Student.builder()
//            .name("Atilla")
//            .surname("Memmedov")
//            .age(21)
//            .address("Qax seh bla bla bla")
//            .email("atilla@gmail.com")
//            .build();
//
//    Student student2 = Student.builder()
//            .name("Mehemmed")
//            .surname("Mehemmedli")
//            .age(21)
//            .email("mehemmed@gmail.com")
//            .address("Gence seh bla bla bla")
//            .build();
//
//    Student student3 = Student.builder()
//            .name("Perviz")
//            .surname("Abishov")
//            .age(24)
//            .email("perviz@gmail.com")
//            .address("Baki seh bla bla bla")
//            .build();
//    Student student4 = Student.builder()
//            .name("Elmir")
//            .surname("Osmanov")
//            .age(28)
//            .email("elmir@gmail.com")
//            .address("Quba seh bla bla bla")
//            .build();
//
//    Student student5 = Student.builder()
//            .name("Suleyman")
//            .surname("Muxtarov")
//            .age(21)
//            .email("suleyman@gmail.com")
//            .address("Lekit kendi bla bla bla")
//            .build();
//    Set<Student> studentsList = Set.of(student,student2,student3,student4,student5);
//
//        studentRepository.saveAll(studentsList);













    }

}
