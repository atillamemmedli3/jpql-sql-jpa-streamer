package az.ingress.ms9.repository;

import az.ingress.ms9.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student, Long> {
    //SQL
    @Query(value = "select * from student where surname like %?1 and address like %?2%"
            ,  nativeQuery = true)
    List<Student> sqlFindStudentByAddressAndSurname(String surname , String address);



    //JPQL
    @Query(value = "select s from Student s where s.surname like %?1% and s.address like %?2%")
    List<Student> jpqlFindStudentByAddressAndSurname(String surname , String address);

}
