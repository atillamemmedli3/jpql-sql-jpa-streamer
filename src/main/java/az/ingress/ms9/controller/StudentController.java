package az.ingress.ms9.controller;

import az.ingress.ms9.dto.StudentDto;
import az.ingress.ms9.model.Student;
import az.ingress.ms9.model.StudentSurnameAddressFind;
import az.ingress.ms9.service.StudentService;
import az.ingress.ms9.service.StudentServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StudentController {

    private final StudentService service;

    public StudentController(StudentService service) {
        this.service = service;
    }

    @GetMapping("/find")
    public List<Student> findStudentByAddressAndSurname(@RequestBody StudentSurnameAddressFind studentSurnameAddressFind){
        return service.getFindStudentByAddressAndSurname(studentSurnameAddressFind.getSurname(), studentSurnameAddressFind.getAddress());
    }

    @GetMapping("/{id}")
    public StudentDto getStudentById(@PathVariable Long id) {
        System.out.println("id"+id);
        return service.getStudentById(id);
    }

    @PostMapping
    public StudentDto createStudent(@RequestBody StudentDto dto) {
        return service.createStudent(dto);
    }

    @PutMapping
    public StudentDto updateStudent(@RequestBody StudentDto dto) {
        return service.updateStudent(dto);
    }

    @DeleteMapping("/{id}")
    public void deleteStudent(@PathVariable Long id) {
        service.deleteStudent(id);
    }




}
