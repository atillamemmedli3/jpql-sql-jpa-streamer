package az.ingress.ms9.controller;

import az.ingress.ms9.dto.StudentDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1")
public class HelloController {

    @GetMapping("/hello")
    public String sayHello(@RequestBody StudentDto dto, @RequestHeader("Accept-Language") String lang) {
        switch (lang) {
            case "en":
                return "Hello from " + dto.getName();
            case "az":
                return dto.getName() + "-dən Salam";
            default:
                return "Language not found";
        }
    }
}
