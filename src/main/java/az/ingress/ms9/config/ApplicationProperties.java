package az.ingress.ms9.config;

import java.util.List;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties("hello")
@Data
@Component
public class ApplicationProperties {

    private String applicationName;
    private Integer version;
    private List<String> developerNames;
}
