package az.ingress.ms9.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StudentSurnameAddressFind {
    String surname;
    String address;
}
