package az.ingress.ms9.service;

import az.ingress.ms9.dto.StudentDto;
import az.ingress.ms9.model.Student;
import az.ingress.ms9.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;
    private final ModelMapper mapper;

    @Override
    public StudentDto getStudentById(Long id) {
        Student byId = studentRepository.getById(id);
        StudentDto dto = mapper.map(byId, StudentDto.class);
        return dto;
    }

    @Override
    public List<Student> getFindStudentByAddressAndSurname(String surname,String address) {
        List<Student> student = studentRepository.sqlFindStudentByAddressAndSurname(surname,address);
        System.out.println(surname+"-impl"+student+" >controller");
        return student;
    }

    @Override
    public StudentDto createStudent(StudentDto dto) {
        Student student = mapper.map(dto, Student.class);
        Student save = studentRepository.save(student);
        return mapper.map(save, StudentDto.class);
    }

    @Override
    public void deleteStudent(Long id) {
        studentRepository.deleteById(id);
    }

    @Override
    public StudentDto updateStudent(StudentDto dto) {
        Student student =
                studentRepository.findById(dto.getId()).orElseThrow((() -> new RuntimeException("Student not found")));
        student.setAge(dto.getAge());
        student.setName(dto.getName());
        Student save = studentRepository.save(student);
        return mapper.map(save, StudentDto.class);
    }






















}
