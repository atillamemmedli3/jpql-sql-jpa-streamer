package az.ingress.ms9.service;

import az.ingress.ms9.dto.StudentDto;
import az.ingress.ms9.model.Student;

import java.util.List;

public interface StudentService {

    StudentDto getStudentById(Long id);

    List<Student> getFindStudentByAddressAndSurname(String surname,String address);

    StudentDto createStudent(StudentDto dto);

    void deleteStudent(Long id);

    StudentDto updateStudent(StudentDto dto);
}
